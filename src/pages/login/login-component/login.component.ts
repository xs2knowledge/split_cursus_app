import { Component } from '@angular/core';
import { NavController, ToastController } from 'ionic-angular';

@Component({
  templateUrl: './login.html'
})
export class LoginComponent {
  account: {username: string, password: string} = {
    username: '',
    password: ''
  };
  
  constructor(
    private navController: NavController,
    private toastController: ToastController) {}

  login() {
    // let loader = this.loadingController.create({
    //   content: "Moment..."
    // });
    // loader.present();

    // this.wordpressService.login(this.account).subscribe((result) => {
    //   // loader.dismiss();
    //   this.storage.set('wordpress.user', result);
    //   this.navController.push(WordpressHome, {
    //     user: result
    //   });
    // }, (error) => {
    //   // loader.dismiss();
    //   let errorMessage = error.json();
    //   if (errorMessage && errorMessage.message) {
    //     let message = errorMessage.message.replace(/<(?:.|\n)*?>/gm, '');
    //     let toast = this.toastController.create({
    //       message: message,
    //       duration: 6000,
    //       position: 'bottom'
    //     });
    //     toast.present();
    //   }
    // });

    // Kijk even naar wordpres login...
    
    let message = 'Succesful Login';
    let toast = this.toastController.create({
        message: message,
        duration: 3000,
        position: 'bottom'
      });
    toast.present();
  }
}
