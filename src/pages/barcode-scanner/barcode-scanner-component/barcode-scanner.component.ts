import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BarcodeScanner, BarcodeScannerOptions } from '@ionic-native/barcode-scanner';
import { Http } from '@angular/http';
import { OnInit } from '@angular/core';
import { CursusCursist } from '../../cursus/cursist/cursus-cursist.component';
import { Config } from '../../../app/app.config';

@Component({
  templateUrl: 'barcode-scanner.html'
})
export class BarcodeScannerComponent implements OnInit {
	barcodeData: any;
	inschrijving: any;
	options: BarcodeScannerOptions;

	constructor(
		public navCtrl: NavController,
		private barcodeScanner: BarcodeScanner,
		private config: Config,
		private http: Http) {
	}

	ngOnInit() {
		this.scan();
	}


	scan() {
		this.barcodeData = "Geen info"
		this.options = {
       prompt : "Scan de barcode van uw inschrijving"
     }
		this.barcodeScanner.scan(this.options).then((result) => {
			this.barcodeData = result.text;
			this.sendBarCode(result.text);
		}, (err) => {
			alert("Scanning failed: " + err);
		});
	}

	sendBarCode(barcode) {
		let url = this.config.splitUrl + '/api/cursus/aanwezigheid?barcode=' + barcode + '&format=json';
		// header("Access-Control-Allow-Origin: *");
		this.http.get(url)
			.map(res => res.json())
      .subscribe(data => {
				this.barcodeData = "result success: " + data;
				this.inschrijving = data;
				this.loadCursist(data)
			},
			(err) => {
				this.barcodeData = "result error: " + err;
			}
		);


	}

	loadCursist(cursist) {
		this.navCtrl.push(CursusCursist, {
			cursist: cursist
		});
	}



}

