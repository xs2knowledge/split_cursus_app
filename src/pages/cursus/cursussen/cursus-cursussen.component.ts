import { Component } from '@angular/core';

import { OnInit } from '@angular/core';
import { NavController, ToastController, LoadingController } from 'ionic-angular';

import { CursusService } from '../shared/services/cursus.service';
import { CursusCursisten } from '../cursisten/cursus-cursisten.component';

@Component({
	templateUrl: './cursus-cursussen.html',
	providers: [ CursusService ]
})
export class CursusCursussen implements OnInit {

	cursussen: any;

	constructor(
		private cursusService: CursusService,
		private navController: NavController,
		private toastController: ToastController,		
		private loadingController: LoadingController) {}

	ngOnInit() {
		this.getCursussen();
	}

	getCursussen() {
		let loader = this.loadingController.create({
			content: "Please wait"
		});
		loader.present();
		
		this.cursusService.getCursussen()
		.subscribe(result => {
			this.cursussen = result;
			loader.dismiss();
    },
    (err) => {
      let toast = this.toastController.create({
        message: "result error: " + err,
        duration: 6000,
        position: 'bottom'
      });
      toast.present();        
			loader.dismiss();
    });
	}

	loadCursus(cursus) {
		this.navController.push(CursusCursisten, {
			cursus: cursus
		});
	}

}
