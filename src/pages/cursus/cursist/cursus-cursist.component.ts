import { Component } from '@angular/core';

import { OnInit } from '@angular/core';
import { NavParams, ToastController, NavController, LoadingController } from 'ionic-angular';
import { Config } from '../../../app/app.config';
import { Http } from '@angular/http';

import { BarcodeScannerComponent } from '../../barcode-scanner/barcode-scanner-component/barcode-scanner.component';

import { CursusService } from '../shared/services/cursus.service';

@Component({
	templateUrl: './cursus-cursist.html',
	providers: [ CursusService ]
})
export class CursusCursist implements OnInit {

	cursussen: any;
	cursist: any;

	constructor(
		private navParams: NavParams,
		private cursusService: CursusService,
		private navController: NavController,
		private toastController: ToastController,
    private config: Config,
    private http: Http,
		private loadingController: LoadingController) {}

	ngOnInit() {
		this.cursist = this.navParams.get('cursist');
	}

  aanwezigheid_bijwerken(cursist) {
    let url = this.config.splitUrl + '/api/cursus/aanwezigheid?cursist_id=' + cursist.id + '&aankomst='+cursist.incheck_tijd+'&vertrek='+cursist.uitcheck_tijd+'&format=json';
    // header("Access-Control-Allow-Origin: *");
    this.http.get(url)
      .map(res => res.json())
      .subscribe(data => {
        this.loadCursist(data)
      },
      (err) => {
        let toast = this.toastController.create({
          message: "result error: " + err,
          duration: 6000,
          position: 'bottom'
        });
        toast.present();        
      }
    );
  }

  nieuweScan() {
    this.navController.push(BarcodeScannerComponent, {});
  }
  updateMediatorCode() {
    let loader = this.loadingController.create({
      content: "Please wait"
    });
    loader.present();

    this.cursusService.updateMediatorCode(this.cursist).subscribe((result) => {
      loader.dismiss();
      this.navController.push(CursusCursist, {
        cursist: result
      });
    }, (error) => {
      loader.dismiss();
      let errorMessage = error.json();
      if (errorMessage && errorMessage.message) {
        let message = errorMessage.message.replace(/<(?:.|\n)*?>/gm, '');
        let toast = this.toastController.create({
          message: message,
          duration: 6000,
          position: 'bottom'
        });
        toast.present();
      } else {
        let toast = this.toastController.create({
          message: 'Onbekende fout opgetreden...',
          duration: 6000,
          position: 'bottom'
        });
        toast.present();      	
      }
    });
  }

  loadCursist(cursist) {
    this.navController.push(CursusCursist, {
      cursist: cursist
    });
  }
}
