import { Component } from '@angular/core';

import { OnInit } from '@angular/core';
import { NavParams, NavController, LoadingController } from 'ionic-angular';

import { CursusService } from '../shared/services/cursus.service';
import { CursusCursist } from '../cursist/cursus-cursist.component';

@Component({
	templateUrl: './cursus-cursisten.html',
	providers: [ CursusService ]
})
export class CursusCursisten implements OnInit {

	cursisten: any;
	cursus: any;
	searchTerm: any;

	constructor(
		private navParams: NavParams,
		private cursusService: CursusService,
		private navController: NavController,
		private loadingController: LoadingController) {}

	ngOnInit() {
		this.cursus = this.navParams.get('cursus');
		this.searchTerm = '';
		this.getCursisten(this.cursus);
	}

	getCursisten(cursus) {
		let loader = this.loadingController.create({
			content: "Please wait"
		});
		loader.present();
		
		this.cursusService.getCursisten(cursus.id, '')
		.subscribe(result => {
			this.cursisten = result;
			loader.dismiss();
		});
	}

	setFilteredItems() {
		this.cursusService.getCursisten(this.cursus.id, this.searchTerm)
		.subscribe(result => {
			this.cursisten = result;
		});
	}

	loadCursist(cursist) {
		this.navController.push(CursusCursist, {
			cursist: cursist
		});
	}

}
