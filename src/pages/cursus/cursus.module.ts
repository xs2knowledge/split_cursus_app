import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../../app/shared/shared.module';
import { CursusCursussen } from './cursussen/cursus-cursussen.component';
import { CursusCursisten } from './cursisten/cursus-cursisten.component';
import { CursusCursist } from './cursist/cursus-cursist.component';

@NgModule({
  declarations: [
    CursusCursussen,
    CursusCursist,
    CursusCursisten
  ],
  imports: [
  	CommonModule,
  	SharedModule
  ],
  exports: [
    CursusCursussen,
    CursusCursist,
    CursusCursisten
  ],
  entryComponents:[
    CursusCursussen,
    CursusCursist,
    CursusCursisten
  ]
})
export class CursusModule {}
