import { Component } from '@angular/core';
import { NavController, Events, MenuController } from 'ionic-angular';

import { WordpressHome } from '../../wordpress/wordpress-home/wordpress-home.component';
import { WordpressLogin } from '../../wordpress/wordpress-login/wordpress-login.component';
import { CursusCursussen } from '../../cursus/cursussen/cursus-cursussen.component';
import { BarcodeScannerComponent } from '../../barcode-scanner/barcode-scanner-component/barcode-scanner.component';
import { ChartsComponent } from '../../charts/charts-component/charts.component';

@Component({
	selector: 'page-home',
	templateUrl: 'home.html'
})
export class HomeComponent {
	pages: Array<{title: string, component: any, icon: string, note: string}>;
	constructor(
		private navController: NavController,
		private menuController: MenuController,
		private events: Events) {}

	ngOnInit() {
	  	this.pages = [
	      { title: 'LOGIN', component: WordpressLogin, icon: 'menu', note: '' },
	      { title: 'CURSUSSEN', component: CursusCursussen, icon: 'menu', note: '' },
	      { title: 'SCANNER', component: BarcodeScannerComponent, icon: 'barcode', note: '' },
	      { title: 'CHARTS', component: ChartsComponent, icon: 'pie', note: 'Chart.js' },
	      { title: 'Wordpres', component: WordpressLogin, icon: 'pie', note: 'Chart.js' },
	      { title: 'Wordpres LI', component: WordpressHome, icon: 'pie', note: 'Chart.js' }
	    ];

	    this.events.subscribe('navigationEvent',(object) => {
	    	this.menuController.close();
			this.navController.push(object.component, object.params);
		});
	}

	openPage(page) {
		this.navController.push(page.component);
	}

}
