Ionic Framework App 1.9.0 comes with Ionic Framework 3.0.1. You can find the project into the ionic-3.0.1 folder.

Install Ionic Framework App
$ npm install -g ionic@latest

Browse the App
$ cd SplitCursusApp

Install package.json dependencies
$ npm install

Install Cordova/PhoneGap plugins (Cordova Plugins package.json branch dependencies)
$ ionic state restore

Test your app on multiple screen sizes and platform types by starting a local development server
$ ionic serve
or
$ ionic serve --lab 

Build iOS
sudo npm install -g ios-deploy --unsafe-perm=true

$ ionic state restore
$ ionic platforms add ios
$ ionic build ios
  cordova build --release ios

Build Android
$ ionic state restore
$ ionic platforms add android
$ ionic build android --prod  --> cordova build --release android

Build Android (Production)
$ ionic state restore
$ ionic platforms add android
$ ionic build android --release --prod

$ cp platforms/android/build/outputs/apk/android-release-unsigned.apk .

$ keytool -genkey -v -keystore split-online.keystore -alias split-online -keyalg RSA -keysize 2048 -validity 10000

$ jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore split-online.keystore android-release-unsigned.apk split-online

$ /Users/marco/Development/android-sdk-mac_86/build-tools/21.1.2/zipalign -v 4 android-release-unsigned.apk split-cursus.apk

cp split-cursus.apk ~/Dropbox/

/Users/gtsopour should be replaced from your user name.

